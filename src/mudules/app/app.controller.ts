import {
  Controller,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes
} from '@nestjs/common';
import {AppService} from './app.service';
import {ApiOperation, ApiParam, ApiResponse, ApiTags} from "@nestjs/swagger";
import {HttpExceptionDto} from "../../dto/httpException.dto";
import {FileInterceptor} from "@nestjs/platform-express";
import {FileValidationPipe} from "../../shared/pipes/fileValidationPipe";
import {HttpSuccessDto} from "../../dto/httpSuccess.dto";

@ApiTags('uploader')
@Controller('upload')
export class AppController {
  constructor(
      private readonly appService: AppService
  ) {}


  @ApiOperation({
    summary: 'Upload image to AWS',
    description:
        'Upload received file to AWS',
  })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: HttpSuccessDto
  })
  @ApiResponse({
    status: 400,
    description: 'Provided data is invalid',
    type: HttpExceptionDto,
  })
  @ApiResponse({
    status: 500,
    description: 'Internal server error occurred',
    type: HttpExceptionDto,
  })
  @ApiParam({
    name: 'fileName',
    required: true,
    description: 'File name to upload',
    schema: { type: 'varchar' },
  })
  @Post('/:fileName')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file'))
  @UsePipes(new FileValidationPipe())
  async upload(
      @Param('fileName') fileName: string,
      @UploadedFile() file: Express.Multer.File
  ): Promise<HttpSuccessDto> {
    if (!file) {
      throw new HttpException('No file provided', HttpStatus.BAD_REQUEST);
    }
    await this.appService.upload({fileName, file});
    return new HttpSuccessDto({statusCode: HttpStatus.CREATED, message: 'Success'});
  }
}
