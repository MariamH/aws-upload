import {BadRequestException, Injectable, Logger} from '@nestjs/common';
import {UploadDto} from "../../dto/upload.dto";
import * as sharp from "sharp"
import {FileSizes} from "../../common/constants";
import {AwsService} from "../../shared/services/aws.service";
import * as fs from "fs";
import {join} from "path";

@Injectable()
export class AppService {
    private readonly logger = new Logger('AwsService')

    constructor(
        public awsService: AwsService,
    ) {
    }

    async upload(uploadDto: UploadDto): Promise<void> {
        const directory = join(__dirname, `/uploads`);
        try {
            const {file, fileName} = uploadDto;
            const {mimetype, buffer, extension} = file;
            if (!fs.existsSync(directory)) {
                fs.mkdirSync(directory);
            }
            const sizes = Object.keys(FileSizes);
            const promises = [];
            sizes.forEach((size: string) => {
                promises.push(
                    sharp(buffer)
                        .resize(FileSizes[size].height, FileSizes[size].width, {
                            fit: sharp.fit.fill
                        })
                        .toFormat(file.extension)
                        .toFile(`${directory}/${fileName}`)
                        .then(() => {
                            this.awsService.uploadToAws({
                                fileName: `${fileName}-${size}.${extension}`,
                                path: `${directory}/${fileName}`,
                                mimeType: mimetype
                            })
                        }).catch((err) => {
                        throw new BadRequestException(err.message);
                    })
                )
            })

            await Promise.allSettled(promises);
            if (!fs.existsSync(directory)) {
                fs.rmdirSync(directory, {recursive: true});
            }
        } catch (e) {
            this.logger.error(e.message);
            if (!fs.existsSync(directory)) {
                fs.rmdirSync(directory, {recursive: true});
            }
            throw new BadRequestException(e.message);
        }
    }
}
