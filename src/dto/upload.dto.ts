import {ApiProperty} from "@nestjs/swagger";
import {IsString} from "class-validator";

export class UploadDto {
    @ApiProperty({ description: 'File name', required: true })
    @IsString({ message: 'File name should be a string' })
    fileName: string;

    @ApiProperty({ description: 'File', required: true })
    file: any;
}
