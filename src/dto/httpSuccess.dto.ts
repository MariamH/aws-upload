import { ApiProperty } from '@nestjs/swagger';

export class HttpSuccessDto {
  @ApiProperty({ description: 'Status code' })
  statusCode: number;

  @ApiProperty({ description: 'Success message' })
  message: string;

  constructor(dto: HttpSuccessDto) {
    this.statusCode = dto.statusCode;
    this.message = dto.message;
  }
}
