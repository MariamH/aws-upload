export enum ValidationPipeMetadataTypes {
    CUSTOM = 'custom',
    PARAM = 'param'
}

export enum PromiseStatuses {
    FULFILLED = 'fulfilled',
    REJECTED = 'rejected'
}