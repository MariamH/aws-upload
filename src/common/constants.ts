export const FileSizes = {
    large: {height: 2048, width: 2048},
    medium: {height: 1024, width: 1024},
    small: {height: 300, width: 300}
}
