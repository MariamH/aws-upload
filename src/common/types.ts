export interface IErrorType {
    error: boolean,
    message: string,
    statusCode: number
}