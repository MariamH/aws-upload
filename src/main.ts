import {NestFactory} from '@nestjs/core';
import {AppModule} from './mudules/app/app.module';
import {INestApplication, ValidationPipe} from "@nestjs/common";
import {setupSwagger} from "./setup-swagger";
import {SharedModule} from "./shared/shared.module";
import {ApiConfigService} from "./shared/services/api-config.service";

export async function bootstrap(): Promise<INestApplication> {
  const app = await NestFactory.create(AppModule);

  const configService = app.select(SharedModule).get(ApiConfigService);
  app.useGlobalPipes(new ValidationPipe())
  const port = configService.appConfig.port;

  if (configService.documentationEnabled) {
    setupSwagger(app);
  }

  await app.listen(port);

  console.info(`server running on port ${port}`);

  return app;
}

void bootstrap();
