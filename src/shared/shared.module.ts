import {Global, Module} from '@nestjs/common';

import {ApiConfigService} from './services/api-config.service';
import {AwsService} from "./services/aws.service";

const providers = [
  ApiConfigService,
  AwsService,
];

@Global()
@Module({
  providers,
  imports: [],
  exports: [
      ...providers
  ],
})
export class SharedModule {}
