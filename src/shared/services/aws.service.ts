import {Injectable, Logger} from "@nestjs/common";
import {ApiConfigService} from "./api-config.service";
import {UploadToAwsDto} from "../../dto/uploadToAws.dto";
import * as AWS from 'aws-sdk'
import * as fs from "fs";


@Injectable()
export class AwsService {
    private readonly s3: AWS.S3;
    private readonly logger = new Logger('AwsService')

    constructor(
        private readonly configService: ApiConfigService,
    ) {
        this.s3 = new AWS.S3({
            accessKeyId: this.configService.awsConfig.access_key,
            secretAccessKey: this.configService.awsConfig.secret_access_key,
        })
    }

    public async uploadToAws(uploadToAwsDto: UploadToAwsDto): Promise<void> {
        try {
            await this.s3.upload({
                Bucket: this.configService.awsConfig.bucket,
                Key: uploadToAwsDto.fileName,
                ContentType: uploadToAwsDto.mimeType,
                Body: fs.createReadStream(uploadToAwsDto.path),
                ACL: "public-read"
            }).promise();
        } catch (e) {
            this.logger.error(e.message);
        }
    }

}
