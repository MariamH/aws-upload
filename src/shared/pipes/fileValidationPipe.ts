import {ArgumentMetadata, BadRequestException, Injectable, PipeTransform} from "@nestjs/common";
import {ConfigService} from "@nestjs/config";
import * as mime from "mime-types"
import {ValidationPipeMetadataTypes} from "../../common/enums";


@Injectable()
export class FileValidationPipe implements PipeTransform {
    private readonly configService = new ConfigService();

    transform(value: any, metadata: ArgumentMetadata) {
        if (metadata.type === ValidationPipeMetadataTypes.PARAM) return value;

        if (value.size > this.configService.get<string>('MAX_FILE_SIZE_BYTES'))
            throw new BadRequestException(`File size exceeds max file size limit`);

        const allowedMimeTypes = this.configService.get<string>('ALLOWED_MIME_TYPES').split('|');
        if (!allowedMimeTypes.includes(value.mimetype))
            throw new BadRequestException(`Provided mimetype is not supported`);

        const extension = mime.extension(value.mimetype);
        const allowedExtensions = this.configService.get<string>('ALLOWED_EXTENSIONS').split('|');
        if (!allowedExtensions.includes(extension))
            throw new BadRequestException(`Provided extension is not supported`);

        return Object.assign(value, {extension});
    }
}